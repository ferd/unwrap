-module(unwrap_SUITE).
-compile([export_all, nowarn_export_all,
          {parse_transform, unwrap}]).
-include_lib("stdlib/include/assert.hrl").

all() ->
    [base, success_unwrap, error_unwrap, nested, trailing,
     badunwrap, flat_only, match_compose, match_ok, unmatch_ok].

%%% UTIL FUNCTION THAT SHOULD BE REPLACED IN SUCCESSFUL CASES %%%
unwrap(_, _) -> original.

%%%%%%%%%%%%%
%%% TESTS %%%
%%%%%%%%%%%%%

base() ->
    [{doc, "ensure base `begin ... end` works as expected"}].
base(_Config) ->
    ?assertEqual(
       0,
       begin
           X = 1,
           Y = 2,
           Z = begin K = -1, K-Y end,
           X+Y+Z
       end
    ).

success_unwrap() ->
    [{doc, "unwrapping all values successfully binds them and lets "
           "the expression succeed"}].
success_unwrap(_Config) ->
    ?assertEqual(
       0,
       begin
           unwrap(X, {ok, 1}),
           unwrap(Y, {ok, 2}),
           Z = begin unwrap(K, {ok, -1}), K-Y end,
           X+Y+Z
       end
    ).

error_unwrap() ->
    [{doc, "encountering an error tuple aborts the begin expression"}].
error_unwrap(_Config) ->
    ?assertEqual(
        {error, 2},
        begin
            unwrap(X, {ok, 1}),
            unwrap(Y, {error, 2}),
            Z = begin unwrap(K, {ok, -1}), K-Y end,
            X+Y+Z
        end
    ).

nested() ->
    [{doc, "nested expressions do work"}].
nested(_Config) ->
    ?assertEqual(
        0,
        begin
            unwrap(X, {ok, 1}),
            unwrap(Y, {ok, 2}),
            {error, Z} = begin unwrap(K, {error, -3}), K-Y end,
            X+Y+Z
        end
    ).

trailing() ->
    [{doc, "transformations in a tail position return that value"}].
trailing(_Config) ->
    ?assertEqual(
       {ok, 1},
       begin unwrap(_, {ok, 1}) end
    ).

badunwrap() ->
    [{doc, "unmatching values are raised properly"}].
badunwrap(_Config) ->
    ?assertException(error, {badunwrap, {ok, 6}},
                     begin unwrap(5, {ok, 6}) end).

flat_only() ->
    [{doc, "only immediate expressions are converted/allowed"}].
flat_only(_Config) ->
    ?assertEqual(
       original,
       begin
           case true of
               true ->
                   unwrap(1, {error, escaped})
           end,
           [unwrap(1, {error, escaped})],
           {unwrap(1, {error, escaped})},
           (fun() -> unwrap(1, {error, escaped}) end)(),
           %% won't compose with = if precedence is off
           original = unwrap(1, {error, escaped})
       end
    ).

match_compose() ->
    [{doc, "will compose with the = operator if precedence is right"}].
match_compose(_Config) ->
    ?assertMatch(
       {{ok, accepted}, accepted},
       begin
           original = unwrap(1, {error, escaped}),
           unwrap(Y, X = {ok, accepted}),
           {X, Y}
       end
    ).

match_ok() ->
    [{doc, "Can unwrap a bare `ok' if no var is bound"}].
match_ok(_Config) ->
    ?assertEqual(
       ok,
       begin unwrap(_, ok) end
    ).

unmatch_ok() ->
    [{doc, "Can't unwrap a bare `ok' if the pattern match isn't `_'"}].
unmatch_ok(_Config) ->
    ?assertException(error, {badunwrap, ok},
                     begin unwrap(X, ok), X end),
    ?assertException(error, {badunwrap, ok},
                     begin unwrap(X = _, ok), X end),
    ?assertException(error, {badunwrap, ok},
                     begin unwrap(_ = X, ok), X end),
    ?assertException(error, {badunwrap, ok},
                     begin unwrap(_ = ok, ok) end),
    ?assertException(error, {badunwrap, ok},
                     begin unwrap(ok = _, ok) end),
    ?assertException(error, {badunwrap, ok},
                     begin unwrap([], ok) end).
