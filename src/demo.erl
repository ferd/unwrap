-module(demo).
-compile([{parse_transform, unwrap},
          export_all, nowarn_export_all]).

ref() ->
    case base() of
        {ok, X} -> X;
        {error, _} = X -> X;
        _ -> error(badunwrap)
    end.

base() ->
    0 =
    begin
        X = 1,
        Y = 2,
        Z = begin K = -1, K-Y end,
        X+Y+Z
    end.

unwrap() ->
    0 =
    begin
        unwrap(X, {ok, 1}),
        unwrap(Y, {ok, 2}),
        Z = begin unwrap(K, {ok, -1}), K-Y end,
        X+Y+Z
    end.

err() ->
    {error, 2} =
    begin
        unwrap(X, {ok, 1}),
        unwrap(Y, {error, 2}),
        Z = begin unwrap(K, {ok, -1}), K-Y end,
        X+Y+Z
    end.

err_nested() ->
    0 =
    begin
        unwrap(X, {ok, 1}),
        unwrap(Y, {ok, 2}),
        {error, Z} = begin unwrap(K, {error, -3}), K-Y end,
        X+Y+Z
    end.

trail() ->
    {ok, 1} =
    begin
        unwrap(_, {ok, 1})
    end.

exception() ->
    try
        begin
            unwrap(5, {ok, 6})
        end
    catch
        error:{badunwrap, {ok, 6}}=E:S -> {error,E,S}
    end.

no_nesting() ->
    ok =
    begin
        case true of %% This construct breaks the flow
            true ->
                unwrap(1, {error, escaped})
        end,
        [unwrap(1, {error, escaped})], %% This construct breaks the flow as well
        {unwrap(1, {error, escaped})}, %% This construct breaks the flow as well
        (fun() -> unwrap(1, {error, escaped}) end)(), %% And so does this
        ok = unwrap(1, {error, escaped}) %% will not compose with match expressions either
    end.

compose_match_lhs() ->
    {{ok, accepted}, accepted} =
    begin
        ok = unwrap(1, {error, escaped}),
        unwrap(Y, X = {ok, accepted}),
        {X, Y}
    end.

allow_ok_atom_on_matchall() ->
    ok = begin
        unwrap(_, ok)
    end.

disallow_ok_atom_on_match() ->
    try
        begin
            unwrap(X, ok),
            X
        end,
        error(failed)
    catch
        error:{badunwrap, ok}=E:S -> {error,E,S}
    end.

%% this function is used to demonstrate lack of
%% syntactical changes applied by the parse transform
unwrap(_, _) ->
    ok.
