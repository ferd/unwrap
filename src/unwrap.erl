-module(unwrap).

%% API exports
-export([parse_transform/2]).
-ifndef(MATCH_OK).
-define(MATCH_OK, true).
-endif.
%%====================================================================
%% API functions
%%====================================================================
parse_transform(ASTs, _Options) ->
    try
        [erl_syntax_lib:map(fun(T) ->
                                transform(erl_syntax:revert(T))
                            end, AST) || AST <- ASTs]
    catch
        _E:_R ->
            ASTs
    end.

%%====================================================================
%% Internal functions
%%====================================================================
%transform(T) ->
%    io:format("~p~n",[T]),
%    T.
transform({block, Line, Ops}) ->
    {block, Line, process_block_ops(Ops)};
transform(T) -> T.

process_block_ops([]) -> [];
process_block_ops([{call, Line, {atom, Line, unwrap}, [Pattern, Exp]} | Rest]) ->
    PatternVar = make_var(),
    [{'case', Line,
      Exp,
      [{clause, Line,
        [{match, Line,
          {tuple, Line, [{atom, Line, ok}, Pattern]},
          {var, Line, PatternVar}}],
        [],
        [{var, Line, PatternVar} | process_block_ops(Rest)]}]
      ++
      [{clause, Line,
        [{atom, Line, ok}],
        [],
        [{atom, Line, ok} | process_block_ops(Rest)]}
       || ?MATCH_OK andalso Pattern == {var, Line, '_'}]
      ++
      [{clause, Line,
        [{tuple, Line, [{atom, Line, error}, {var, Line, PatternVar}]}],
        [],
        [{tuple, Line, [{atom, Line, error}, {var, Line, PatternVar}]}]},
       {clause, Line,
        [{var, Line, PatternVar}],
        [],
        [{call, Line, {atom, Line, error},
          [{tuple, Line, [{atom, Line, badunwrap}, {var, Line, PatternVar}]}]
         }]}
      ]}];
process_block_ops([H|Rest]) ->
    [H|process_block_ops(Rest)].

make_var() ->
    list_to_atom(lists:flatten(io_lib:format("~p",[make_ref()]))).
